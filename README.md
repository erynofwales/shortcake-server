Shortcake
=========

The server component of Shortcake, a URL shortener and file hosting platform.

## Authors

* Eryn Wells <eryn@erynwells.me>
