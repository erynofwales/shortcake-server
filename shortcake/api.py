import logging

from django.conf.urls import url
from tastypie import authentication as authc
from tastypie import authorization as authz
from tastypie import fields
from tastypie.api import Api
from tastypie.models import ApiKey
from tastypie.resources import ModelResource, Resource
from tastypie.utils import trailing_slash


LOG = logging.getLogger(__name__)


class PerUserAuthentication(authc.ApiKeyAuthentication):
    '''
    API key authentication that filters the object list to just those objects
    the user is allowed to access.
    '''
    def read_list(self, object_list, bundle):
        return object_list.filter(user=bundle.request.user)


class PerUserResource(object):
    '''
    Per-user resources require an authenticated request and expect the model
    object to have a user field.
    '''
    class Meta:
        authentication = PerUserAuthentication()
        authorization = authz.DjangoAuthorization()

    def hydrate(self, bundle):
        bundle.obj.user = bundle.request.user
        return bundle


class ShortURLResource(object):
    hits = fields.IntegerField(readonly=True,
            help_text='The number of hits this URL has received.')
    short_url = fields.CharField(readonly=True, help_text='The short URL.')

    def dehydrate_hits(self, bundle):
        return bundle.obj.url.hits_count

    def dehydrate_short_url(self, bundle):
        return bundle.obj.url.get_short_url(bundle.request)


class DetailOnlyResource(object):
    '''
    Detail-only resources have not 'list' endpoint, and allow only get requests
    on the detail endpoint.
    '''
    class Meta:
        list_allowed_methods = []
        detail_allowed_methods = ['get']

    def base_urls(self):
        rname_regex = r'(?P<resource_name>{})'.format(self._meta.resource_name)
        return [
            url(r'^{}{}$'.format(rname_regex, trailing_slash()),
                    self.wrap_view('dispatch_detail'),
                    name='api_dispatch_detail'),
            url(r'^{}/schema{}$'.format(rname_regex, trailing_slash()),
                    self.wrap_view('get_schema'), name='api_get_schema')
        ]


class HelloResource(Resource, DetailOnlyResource):
    username = fields.CharField(help_text='Your username.')
    api_key = fields.CharField(readonly=True,
            help_text='The API key to use for further requests against this '
                      'API. This key can be used in place of basic auth, if '
                      'desired. Add an api_key GET parameter to each request '
                      'to authenticate this way.')

    class Meta(DetailOnlyResource.Meta):
        authentication = authc.BasicAuthentication()
        authorization = authz.DjangoAuthorization()
        include_resource_uri = False

    def obj_get(self, bundle, **kwargs):
        # No object required here. This just suppresses the NotImplemented
        # exception in super.get_obj().
        pass

    def dehydrate_api_key(self, bundle):
        # An API key should already exist for this user because I've got the
        # signals hooked up. If it doesn't, create one (but complain about it).
        apikey, created = ApiKey.objects.get_or_create(user=bundle.request.user)
        if created:
            LOG.warning('Created API key for {}; this should have been done '
                        'already...'.format(bundle.request.user.username))
        return apikey.key

    def dehydrate_username(self, bundle):
        return bundle.request.user.username


def create_v1_api():
    api = Api(api_name='shortcake')
    api.register(HelloResource())
    return api
