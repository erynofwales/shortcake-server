from django.conf import settings
from django.conf.urls import patterns, include, url
from django.conf.urls.static import static
from django.contrib import admin

import shortcake.api
import shortcake.views
import shortener.api
import uploader.api


admin.autodiscover()

urlpatterns = patterns('',
    url(r'^admin/', include(admin.site.urls)),

    url(r'^api/v1/', include(shortener.api.create_v1_api().urls)),
    url(r'^api/v1/', include(shortcake.api.create_v1_api().urls)),
    url(r'^api/v1/', include(uploader.api.create_v1_api().urls)),

    url(r'^web/$', shortcake.views.HomeView.as_view(), name='home'),
    url(r'^web/login/$', 'django.contrib.auth.views.login',
        {'template_name': 'login.html'}, name='login'),

    url(r'^(?P<hash>[a-zA-Z0-9]+)$', shortcake.views.ShortURLView.as_view(),
        name='shorturl')
)

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
