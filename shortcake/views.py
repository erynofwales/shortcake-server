from django.conf import settings
from django.http import Http404
from django.shortcuts import get_object_or_404
from django.views.generic.base import TemplateView, View
from tastypie.models import ApiKey

import shortener.views
import uploader.views
from shortener.models import URL, Hit
from uploader.models import File


class HomeView(TemplateView):
    template_name = 'home.html'

    def get_context_data(self, **kwargs):
        context = super(HomeView, self).get_context_data(**kwargs)
        context['settings'] = settings
        context['username'] = self.request.user.username
        context['api_key'] = ApiKey.objects.get(user=self.request.user).key
        return context


class ShortURLView(View):
    '''
    A lightweight view that handles the common stuff for short URLs, and then
    dispatches to the proper view for actual handling.
    '''

    def dispatch(self, request, *args, **kwargs):
        url_hash = kwargs['hash']
        try:
            url = URL.objects.get_with_hash(url_hash)
        except URL.DoesNotExist as e:
            raise Http404

        # Register the hit.
        meta = self.request.META
        Hit.objects.create(url=url,
                           http_host=meta.get('HTTP_HOST'),
                           http_referer=meta.get('HTTP_REFERER'),
                           http_user_agent=meta.get('HTTP_USER_AGENT'),
                           remote_addr=meta.get('REMOTE_ADDR'),
                           remote_host=meta.get('REMOTE_HOST'),
                           remote_user=meta.get('REMOTE_USER'),
                           server_name=meta.get('SERVER_NAME'),
                           server_port=meta.get('SERVER_PORT'))

        # Figure out how to handle this view. If it points to a File, we need to
        # show Shortcake's viewer page; otherwise, redirect to the target_url.
        try:
            url_file = File.objects.get(url=url)
        except File.DoesNotExist:
            view = shortener.views.Redirect.as_view()
        else:
            view = uploader.views.Viewer.as_view()
            self.kwargs['file'] = url_file
        finally:
            self.kwargs['url'] = url

        return view(self.request, *self.args, **self.kwargs)
