(function() {
    var app = angular.module('shortcake', []);

    app.controller('URLController', ['$scope', '$http', function($scope, $http) {
    }]);

    app.directive('urlTable', function() {
        return {
            restrict: 'E',
            templateUrl: '/static/js/templates/url_table.html',
            controller: function($scope, $http) {
                this.nextURL = null;
                this.previousURL = null;

                this.urls = $http.get(api.url, {
                    params: {
                        username: username,
                    api_key: apiKey
                    }
                }).success(function(data) {
                    $scope.nextURL = data.meta.next;
                    $scope.previousURL = data.meta.previous;
                    $scope.urls = data.objects;
                });

                this.hasNext = function() {
                    return this.nextURL !== null;
                };

                this.getNext = function() {
                    if (!this.hasNext()) {
                        return;
                    }

                    $http.get(this.nextURL, {
                        params: {username: username, api_key: apiKey}
                    }).success(function(data) {
                        $scope.nextURL = data.meta.next;
                        $scope.urls = $scope.urls.concat(data.meta.objects);
                    });
                };

                this.hasPrevious = function() {
                    return this.previousURL !== null;
                };

                this.getPrevious = function() {
                    if (!this.hasPrevious()) {
                        return;
                    }

                    $http.get(this.previousURL, {
                        params: {username: username, api_key: apiKey}
                    }).success(function(data) {
                        $scope.previousURL = data.meta.previous;
                        $scope.urls = data.objects.concat($scope.urls);
                    });
                };
            },
            controllerAs: 'urlCtrl'
        };
    });
})();
