Shortcake TODOs
===============

## General

* Internal UI

  There currently isn't a UI (other than the Django admin UI) for managing URLs
  and files.

* Twitter OAuth echo authentication

  TweetBot sends these headers with its requests to custom APIs for URL
  shortening and image uploading. It'd be nice to be able to use these instead
  of having to give the API key in the URL.

  See https://dev.twitter.com/docs/auth/oauth/oauth-echo

* Filesystem monitor for uploaded files

  A daemon or task (Celery?) for monitoring the media directory and
  automatically creating URL and File objects for newly added files. This
  process could also clean up files that are removed.

## File Uploads

* Upload API

## Viewer

* Browser supported mimetype detection

  Browsers support different mimetypes for audio and video. We should to detect
  this on the client and adjust accordingly.

* File details tab/panel

  A collapsable panel below the file that shows extra details like size,
  dimensions, mimetype, hash maybe
