from django.db.models import Q
from django.http import Http404
from django.views.generic import RedirectView

from shortener.models import URL, Hit


class Redirect(RedirectView):
    http_method_names = ['get', 'head']
    query_string = False
    permanent = False

    def get_redirect_url(self, *args, **kwargs):
        url_hash = kwargs['hash']
        try:
            url = URL.objects.get_with_hash(url_hash)
        except URL.DoesNotExist as e:
            raise Http404

        return url.target_url
