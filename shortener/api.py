from django.conf.urls import url
from tastypie import fields
from tastypie import http
from tastypie.api import Api
from tastypie.authorization import DjangoAuthorization
from tastypie.exceptions import ImmediateHttpResponse
from tastypie.resources import ModelResource, Resource
from tastypie.utils import trailing_slash

from shortcake.api import (
        PerUserAuthentication, PerUserResource, ShortURLResource,
        DetailOnlyResource)
from shortener.models import URL


class URLResource(PerUserResource, ShortURLResource, ModelResource):
    # TODO: Is there a way to programatically get the help_text for a particular
    # field that isn't totally disgusting? (URL._meta.fields is a list, not a
    # dict.)
    private_hash = fields.CharField(attribute='private_hash', readonly=True,
            unique=True,
            help_text='A long hash that can be used to redirect to the '
                      'target URL. Shortened URLs marked private are always '
                      'refered to by this hash. Public URLs may also be '
                      'refered to by this hash.')
    public_hash = fields.CharField(attribute='public_hash', readonly=True,
            unique=True,
            help_text='A short hash that can be used to redirect to the '
                      'target URL. Shortened URLs marked public (not private) '
                      'are refered to by this hash.')

    target_url = fields.CharField(attribute='target_url',
            help_text='The URL to redirect to, shortened by this API.')

    class Meta(PerUserResource.Meta):
        queryset = URL.objects.all()
        excludes = ('id',)
        include_resource_uri = False
        always_return_data = True

    def obj_create(self, bundle, **kwargs):
        try:
            url = bundle.data['target_url']
        except KeyError:
            raise ImmediateHttpResponse(response=http.HttpBadRequest())

        try:
            bundle.obj = URL.objects.get(target_url=url,
                    user=bundle.request.user)
        except URL.DoesNotExist:
            bundle.obj = URL(**kwargs)
        except self._meta.object_class.MultipleObjectsReturned:
            raise ImmediateHttpResponse(response=http.HttpBadRequest())
        bundle = self.full_hydrate(bundle)
        return self.save(bundle)

    def dehydrate_hits(self, bundle):
        return bundle.obj.hits_count

    def dehydrate_short_url(self, bundle):
        return bundle.obj.get_short_url(bundle.request)


class TweetbotURLResource(ModelResource, PerUserResource, DetailOnlyResource):
    shorturl = fields.CharField(readonly=True,
            help_text='The short URL. This field is equivalent to the '
                      'short_url field, but is provided for compatibility '
                      'with TweetBot.')

    url = fields.CharField(attribute='target_url',
            help_text='The URL to redirect to, shortened by this API. This '
                      'field is equivalent to the target_url field, but is '
                      'provided for compatibility with TweetBot. Either this '
                      'or the `target_url` field must be filled in for POST '
                      'requests.')

    class Meta(DetailOnlyResource.Meta, PerUserResource.Meta):
        resource_name = 'tweetbot/url'
        include_resource_uri = False

    def obj_get(self, bundle, **kwargs):
        request_get = bundle.request.GET
        url = None
        if 'url' in request_get:
            url = request_get['url']
        elif 'u' in request_get:
            url = request_get['u']
        else:
            raise ImmediateHttpResponse(response=http.HttpBadRequest())

        try:
            url_obj, _ = URL.objects.get_or_create(
                    target_url=url, user=bundle.request.user)
        except URL.MultipleObjectsReturned:
            raise ImmediateHttpResponse(response=http.HttpApplicationError())

        return url_obj

    def dehydrate_shorturl(self, bundle):
        return bundle.obj.get_short_url(bundle.request)


def create_v1_api():
    api = Api(api_name='shortener')
    api.register(URLResource())
    api.register(TweetbotURLResource())
    return api
