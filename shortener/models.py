import random
import string

import requests
from bs4 import BeautifulSoup
from django.contrib.auth.models import User
from django.dispatch import receiver
from django.db import models
from django.db.models import Q
from tastypie.models import create_api_key as tastypie_create_api_key


HASH_CHARS = string.letters + string.digits


class URLManager(models.Manager):
    def get_with_hash(self, hash):
        url = super(URLManager, self).get(  Q(public_hash=hash)
                                          | Q(private_hash=hash))

        # Private shortened URLs can only be referred to by their private hash.
        # Raise a 404 error if we fetched a URL by its public hash but the URL
        # isn't public.
        #
        # Note that public URLs can still be referred to by their private hash.
        if url.private and hash == url.public_hash:
            raise URL.DoesNotExist()

        return url


class URL(models.Model):
    date_added = models.DateTimeField(auto_now_add=True)

    public_hash = models.CharField(max_length=255, unique=True, blank=True,
            help_text='A short hash that can be used to redirect to the '
                      'target URL. Shortened URLs marked public (not private) '
                      'are refered to by this hash.')
    private_hash = models.CharField(max_length=255, unique=True, blank=True,
            help_text='A long hash that can be used to redirect to the '
                      'target URL. Shortened URLs marked private are always '
                      'refered to by this hash. Public URLs may also be '
                      'refered to by this hash.')

    target_url = models.URLField(unique=True, blank=True, null=True,
            verbose_name='Target URL', help_text='The URL to redirect to.')

    private = models.BooleanField(default=False,
            help_text='Mark the shortened URL as private. A private URL is '
                      'always referred to by its long hash.')

    #pinned = models.BooleanField(default=False,
    #        help_text='Mark the shortened URL as pinned. A pinned URL that is '
    #                  'not marked private will retain its short hash, even if '
    #                  'a newer URL with the same short hash is created.')

    #trashed = models.BooleanField(default=False)

    title = models.CharField(max_length=255, blank=True,
            help_text='A title for the shortened URL. If no title is given, '
                      'the URL will be fetched and if the fetched data is HTML '
                      'and a <code>&lt;title&gt;</code> tag is present, it '
                      'will be used.')
    description = models.TextField(blank=True,
            help_text='A description for the shortened URL. If no description '
                      "is given, it will be extracted from the document's "
                      '<code>&lt;meta&gt;</code> tags if they exist.')

    user = models.ForeignKey(User)

    objects = URLManager()

    class Meta:
        verbose_name = 'URL'

    @property
    def url_hash(self):
        return self.private_hash if self.private else self.public_hash

    @property
    def hits_count(self):
        return self.hits.count()

    def generate_hash(self, length, field_name=None):
        kw = dict([(field_name, None)]) if field_name else None
        while True:
            new_hash = ''.join(random.sample(HASH_CHARS, length))
            # Check if a hash already exists. If not, return the new hash.
            if kw:
                if self.__class__.objects.filter(**kw).count() == 0:
                    return new_hash
            else:
                return new_hash

    def generate_public_hash(self):
        # TODO: Public hash length should be configurable.
        return self.generate_hash(4, 'public_hash')

    def generate_private_hash(self):
        # TODO: Private hash length should be configurable.
        return self.generate_hash(24, 'private_hash')

    def get_absolute_url(self):
        return '/{}'.format(self.url_hash)

    def get_short_url(self, request):
        '''
        Given an HTTP request object, produce the fully-qualified, absolute HTTP
        URL that represents this URL.
        '''
        # Always use HTTP for short URLs.
        # XXX: This is a HACK because I can't afford an SSL certificate.
        # Ideally, it shouldn't matter whether the connection is encrypted or
        # not.
        short_url = request.build_absolute_uri(self.get_absolute_url())
        if short_url.startswith('https://'):
            short_url = short_url.replace('https', 'http', 1)
        return short_url

    def __unicode__(self):
        return self.title if self.title else self.target_url


class Hit(models.Model):
    date = models.DateTimeField(auto_now_add=True)

    http_host = models.CharField(max_length=255, blank=True, null=True,
            verbose_name='HTTP Host',
            help_text='The HTTP Host header from the client.')
    http_referer = models.CharField(max_length=255, blank=True, null=True,
            verbose_name='HTTP Referer',
            help_text='The HTTP Referer header from the client.')
    http_user_agent = models.CharField(max_length=255, blank=True, null=True,
            verbose_name='User Agent',
            help_text='The user agent string from the client.')
    remote_addr = models.CharField(max_length=255, blank=True, null=True,
            verbose_name='Client IP',
            help_text='The IP address of the client.')
    remote_host = models.CharField(max_length=255, blank=True, null=True,
            verbose_name='Client Hostname',
            help_text='The host name of the client.')
    remote_user = models.CharField(max_length=255, blank=True, null=True,
            verbose_name='User',
            help_text='The user authenticated by the web server, if any.')
    server_name = models.CharField(max_length=255, blank=True, null=True,
            verbose_name='Server',
            help_text='The hostname of the server.')
    server_port = models.CharField(max_length=255, blank=True, null=True,
            verbose_name='Port',
            help_text='The port of the server.')

    url = models.ForeignKey(URL, related_name='hits')

    def __unicode__(self):
        return '{} on {}'.format(self.url, self.date)


@receiver(models.signals.pre_save, sender=URL)
def generate_url_hashes(sender, **kwargs):
    instance = kwargs['instance']
    if not instance.public_hash:
        instance.public_hash = instance.generate_public_hash()
    if not instance.private_hash:
        instance.private_hash = instance.generate_private_hash()


@receiver(models.signals.pre_save, sender=URL)
def retrieve_title_and_description(sender, **kwargs):
    instance = kwargs['instance']

    if not instance.target_url:
        # No target URL. This is probably for a file upload or text blob.
        return

    if instance.title or instance.description:
        return

    should_process = True

    # Fetch the page.
    req = requests.get(instance.target_url)
    if req.status_code != 200:
        should_process = False

    # Only process HTML documents.
    # TODO: Not actually sure what this should be.
    if 'text' not in req.headers['content-type']:
        should_process = False

    if not should_process:
        return

    soup = BeautifulSoup(req.text)

    if not instance.title:
        instance.title = soup.title.text

    if not instance.description:
        def meta_descriptions(tag):
            if tag.name != 'meta':
                return False
            if tag.attrs.get('name', '').lower() != 'description':
                return False
            return True

        content_tags = (m['content'] for m in soup.find_all(meta_descriptions))
        instance.description = ' --- '.join(content_tags)


# Register Tastypie's create_api_key function to automatically create API keys
# for new users.
models.signals.post_save.connect(tastypie_create_api_key, sender=User)
