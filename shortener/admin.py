from django.contrib import admin
from shortener.models import Hit, URL


class HitInlineAdmin(admin.TabularInline):
    model = Hit
    ordering = ('date',)
    can_delete = False
    extra = 0
    max_num = 0

    readonly_fields = (
        'date',
        'http_host', 'http_referer', 'http_user_agent',
        'remote_addr', 'remote_host', 'remote_user',
        'server_name', 'server_port',
    )


class URLAdmin(admin.ModelAdmin):
    fieldsets = (
        (None, {
            'fields': ('target_url', 'title', 'description', 'private'),
        }),
        ('Hashes', {
            'fields': ('public_hash', 'private_hash'),
        })
    )

    readonly_fields = ('public_hash', 'private_hash')

    inlines = (HitInlineAdmin,)

    list_display = ('__unicode__', 'target_url', 'short_url', 'hits')

    def short_url(self, obj):
        return obj.get_absolute_url()
    short_url.short_description = 'Short URL'

    def hits(self, obj):
        return obj.hits.count()

    def save_model(self, request, obj, form, change):
        obj.user = request.user
        obj.save()


admin.site.register(URL, URLAdmin)
