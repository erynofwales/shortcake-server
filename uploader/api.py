from tastypie import fields
from tastypie import http
from tastypie.api import Api
from tastypie.exceptions import ImmediateHttpResponse
from tastypie.resources import ModelResource
from tastypie.utils import dict_strip_unicode_keys

from shortcake.api import (
        PerUserAuthentication, PerUserResource, ShortURLResource,
        DetailOnlyResource)
from uploader.models import File


class FileResource(PerUserResource, ShortURLResource, ModelResource):
    file = fields.FileField(attribute='file', readonly=True,
            help_text='The direct link to the file.')
    hash = fields.CharField(attribute='hash',
            help_text='The SHA-256 hash of the file.')
    mimetype = fields.CharField(attribute='mimetype',
            help_text='The mimetype (e.g. text/html).')

    title = fields.CharField(help_text='A title for the uploaded file.')
    description = fields.CharField(
            help_text='A description for the upload. This will be used as the '
                      'alt text for images.')

    class Meta(PerUserResource.Meta):
        queryset = File.objects.all()
        excludes = ('id',)
        include_resource_uri = False
        always_return_data = True

    def dehydrate_title(self, bundle):
        return bundle.obj.url.title

    def dehydrate_description(self, bundle):
        return bundle.obj.url.description

    def hydrate(self, bundle):
        bundle = super(FileResource, self).hydrate(bundle)
        return bundle

    def hydrate_title(self, bundle):
        bundle.obj.url.title = bundle.data.get('title')
        return bundle

    def hydrate_description(self, bundle):
        bundle.obj.url.description = bundle.data.get('description')
        return bundle


class POSTUploadResource(PerUserResource, DetailOnlyResource):
    class Meta(DetailOnlyResource.Meta, PerUserResource.Meta):
        include_resource_uri = False
        detail_allowed_methods = ['post']

    def post_detail(self, request, **kwargs):
        '''
        Do the POST request work. This method is essentially identical to
        Tastypie's post_list Resource method with a few changes; as much as
        possible it tries to keep the same semantics and method calls. This
        resource expects a multipart POST request, so it doesn't bother to
        deserialize from JSON.
        '''
        fields = dict_strip_unicode_keys(self.fields_from_request(request))
        stripped_kwargs = self.remove_api_resource_names(kwargs)

        bundle = self.build_bundle(data=fields, request=request)
        updated_bundle = self.obj_create(bundle, **stripped_kwargs)
        location = self.get_resource_uri(updated_bundle)

        updated_bundle = self.full_dehydrate(updated_bundle)
        updated_bundle = self.alter_detail_data_to_serialize(request, updated_bundle)
        return self.create_response(request, updated_bundle,
                response_class=http.HttpCreated, location=location)

    def fields_from_request(self, request):
        raise NotImplementedError()

    def obj_create(self, bundle, **kwargs):
        # TODO: Do something with message and source.
        file_hash = File.calculate_hash(bundle.data['media'])
        try:
            bundle.obj = File.objects.get(hash=file_hash)
        except File.DoesNotExist:
            bundle.obj = File(file=bundle.data['media'],
                              user=bundle.request.user)
        except File.MultipleObjectsReturned:
            raise ImmediateHttpResponse(response=http.HttpApplicationError())

        return self.save(bundle)


class TweetbotFileResource(POSTUploadResource, ModelResource):
    url = fields.CharField(readonly=True,
            help_text='The URL pointing to the uploaded media.')

    #message = fields.CharField()
    #source = fields.CharField()
    # TODO: THIS IS WRONG
    #media = fields.CharField()

    class Meta(DetailOnlyResource.Meta, PerUserResource.Meta):
        queryset = File.objects.all()
        resource_name = 'tweetbot/file'
        include_resource_uri = False
        detail_allowed_methods = ['post']

    def fields_from_request(self, request):
        return {
            'message': request.POST.get('message'),
            'source': request.POST.get('source'),
            'media': request.FILES.get('media'),
        }

    def dehydrate_url(self, bundle):
        return bundle.obj.url.get_short_url(bundle.request)


def create_v1_api():
    api = Api(api_name='uploader')
    api.register(FileResource())
    api.register(TweetbotFileResource())
    return api
