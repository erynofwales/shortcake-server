import datetime
import hashlib
import logging
import magic
import os

from django.conf import settings
from django.contrib.auth.models import User
from django.db import models
from django.dispatch import receiver

from shortener.models import URL


LOG = logging.getLogger('uploader')


def file_upload_path(instance, filename):
    return '{}/{}'.format(instance.url.private_hash, filename)


class File(models.Model):
    # A list of hash algorithms to use for generating hashes of files.
    HASH_ALGORITHMS = ['sha256', 'md5']

    # The active hash algorithm.
    HASH_ALGORITHM = None

    @classmethod
    def choose_hash_algorithm(cls):
        '''
        Try to choose a hash algorithm using the UPLOADER_HASH_ALGORITHM
        setting, if specified, and the HASH_ALGORITHMS list on this model.
        '''
        if File.HASH_ALGORITHM:
            return File.HASH_ALGORITHM

        try:
            alg = settings.UPLOADER_HASH_ALGORITHM
        except AttributeError:
            LOG.warning('No hash algorithm specified in settings. Use '
                        'settings.UPLOADER_HASH_ALGORITHM to choose an '
                        'algorithm.')
            alg = None

        # Make sure we choose one that's available.
        if alg not in hashlib.algorithms:
            LOG.warning('Hash algorithm specified in settings.UPLOADER_HASH_ALGORITHM '
                        'is not available. Trying some others.')
            for proposed_alg in File.HASH_ALGORITHMS:
                if proposed_alg in hashlib.algorithms:
                    alg = proposed_alg
                    break
            else:
                LOG.error('No hash algorithm found. Something is very broken. :(')
                return None

        LOG.info('Using %s to compute file hashes.', alg)
        File.HASH_ALGORITHM = alg
        return alg

    @classmethod
    def calculate_hash(cls, f):
        '''
        Calculate the has of the provided buffer (the result of file.read() or
        the like) and return the hex digest of that hash.
        '''
        try:
            alg = hashlib.new(File.choose_hash_algorithm())
        except TypeError:
            LOG.error('Cannot compute file hashes!')
            return None

        # Generate a hash with the file contents.
        # XXX: This could choke on big files, if they're big enough.
        alg.update(f.read())

        return alg.hexdigest()

    hash = models.CharField(max_length=255, unique=True)
    file = models.FileField(upload_to=file_upload_path)
    mimetype = models.CharField(max_length=255, blank=True)
    url = models.OneToOneField(URL, blank=True, null=True, verbose_name='URL',
            on_delete=models.SET_NULL)

    user = models.ForeignKey(User)

    def get_absolute_url(self):
        return self.url.get_absolute_url()

    def __unicode__(self):
        return self.file.url


@receiver(models.signals.pre_save, sender=File)
def ensure_url(sender, **kwargs):
    '''Make sure a URL object exists for this file.'''
    if kwargs.get('raw'):
        # The raw argument means the model should be saved exactly as presented,
        # and no queries of other database objects, or modifications to the
        # database should be performed.
        return

    instance = kwargs['instance']

    if instance.url:
        return

    instance.url = URL.objects.create(user=instance.user)


@receiver(models.signals.pre_save, sender=File)
def get_file_hash_and_mimetype(sender, **kwargs):
    instance = kwargs.get('instance')

    # This is some really bad semantics on Django's part. FieldField.open() does
    # not return a file object (it returns None), so it isn't a valid context
    # manager. FieldFile is a django.core.files.File object, which *is* a valid
    # context manager. Unfortunately, the __enter__ method doesn't actually open
    # the file, like Python standard file context managers do. So, we have to
    # create the context manager (the 'with' statement), open the file, and
    # *then* read it. The context manager will close the file afterwards.
    with instance.file as f:
        f.open()
        instance.hash = File.calculate_hash(f.file)
        LOG.debug("Hash of uploaded file: %s", instance.hash)

        # Rewind to the beginning (just in case) and do mimetype detection.
        f.seek(0)
        instance.mimetype = magic.from_buffer(f.read(1024), mime=True)
        LOG.debug("Mimetype of uploaded file: %s", instance.mimetype)


@receiver(models.signals.post_save, sender=File)
def update_url_title(sender, **kwargs):
    '''Set the URL title and description to something more helpful.'''
    instance = kwargs['instance']
    
    title_desc = 'Uploaded file: {}'.format(instance.file.name)
    instance.url.title = title_desc
    instance.url.description = title_desc
    instance.url.save()


@receiver(models.signals.pre_delete, sender=File)
def clean_uploaded_file(sender, **kwargs):
    instance = kwargs.get('instance')

    file_dir = os.path.dirname(instance.file.path)

    # Clean up the filesystem. Don't save the object since it's going away
    # anyway.
    instance.file.delete(save=False)

    try:
        # The directory should be empty now. Remove it.
        os.rmdir(file_dir)
    except OSError as e:
        LOG.warning('Error removing containing directory for uploaded file: %s',
                    e.strerror)
    else:
        LOG.info('Removed containing directory for uploaded file: %s', file_dir)

    try:
        instance.url.delete()
    except AttributeError:
        # TODO: Log an error. Shouldn't get here in a sane environment.
        pass
