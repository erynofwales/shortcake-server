import os.path
from django.views.generic import DetailView
from uploader.models import File


class Viewer(DetailView):
    model = File
    template_name = 'uploader/viewer.html'

    def get_context_data(self, **kwargs):
        context = super(Viewer, self).get_context_data(**kwargs)

        context['show_brand'] = True

        context['url'] = self.kwargs.get('url')
        context['hostname'] = self.request.META.get('SERVER_NAME')

        # Split up the mime type and subtype to make it easier for the
        # templates.
        types = self.object.mimetype.split('/', 1)
        context['mime_type'] = types[0]
        context['mime_subtype'] = types[1]

        context['basename'] = os.path.basename(self.object.file.path)

        context['video_mime_subtypes'] = ['mpeg', 'mp4', 'ogg', 'webm']
        context['audio_mime_subtypes'] = ['mpeg', 'ogg', 'wav']

        return context

    def get_object(self, queryset=None):
        return self.kwargs['file']
