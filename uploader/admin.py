from django.contrib import admin
from shortener.models import URL
from uploader.models import File


class FileAdmin(admin.ModelAdmin):
    readonly_fields = ('hash', 'mimetype')

    list_display = ('__unicode__', 'hits')

    def hits(self, obj):
        try:
            return obj.url.hits.count()
        except AttributeError:
            return -1

    def save_model(self, request, obj, form, change):
        obj.user = request.user
        obj.save()


admin.site.register(File, FileAdmin)
